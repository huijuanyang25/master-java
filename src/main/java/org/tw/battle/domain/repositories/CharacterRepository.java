package org.tw.battle.domain.repositories;

import org.tw.battle.domain.ServiceConfiguration;
import org.tw.battle.infrastructure.DatabaseConnectionProvider;

import java.sql.*;

import static org.tw.battle.infrastructure.DatabaseConnectionProvider.createConnection;

/**
 * @author Liu Xia
 */
public class CharacterRepository {
    private final ServiceConfiguration configuration;

    public CharacterRepository(ServiceConfiguration configuration) {
        this.configuration = configuration;
    }

    public int create(String name, int hp, int x, int y) throws Exception {
        // TODO:
        //   Please implement the method.
        //   Please refer to DatabaseConnectionProvider to see how to create connection.
        try (
                Connection con = createConnection(configuration);
                PreparedStatement preparedStatement = con.prepareStatement(
                        "INSERT INTO character(name, hp, x, y) VALUES (?, ?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
        ) {
            preparedStatement.setString(1, name);
            preparedStatement.setInt(2, hp);
            preparedStatement.setInt(3, x);
            preparedStatement.setInt(4, y);
            preparedStatement.execute();

            final ResultSet resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            return resultSet.getInt(1);
        }
    }

    public String findCharacter(int id) throws Exception {
        String message;
        try (
                Connection con = createConnection(configuration);
                Statement statement = con.createStatement();
                ) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM character WHERE id = " + id);
            if (resultSet.next()) {
                int findId = resultSet.getInt(1);
                String findName = resultSet.getString(2);
                int findHp = resultSet.getInt(3);
                int findX = resultSet.getInt(4);
                int findY = resultSet.getInt(5);
                message = String.format("id: %d, name: %s, hp: %d, x: %d, y: %d, status: %s",
                        findId, findName, findHp, findX, findY, findHp > 0 ? "alive" : "dead");
            } else {
                message = "Bad command: character not exist";
            }
        }
        return message;
    }

    public String renameCharacter(int id, String name) throws Exception {
        String message;
        try (
                Connection con = createConnection(configuration);
                Statement statement = con.createStatement();
                ) {
            String querySQL = "UPDATE character SET name = " + name + " WHERE id = " + id;
            ResultSet resultSet = statement.executeQuery(querySQL);
            if (statement.executeUpdate(querySQL) > 0) {
                message = String.format("Character renamed: id: %d, name: %s", id, name);
            } else {
                message = "Bad command: rename-character <character id><name>";
            }
        }
        return message;
    }
}
